﻿using System;
using MiniGolf2d.Assets.Code.AllManagers;
using MiniGolf2d.Assets.Code.UI;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System.Collections;

namespace Code {
    public class TitleText : MonoBehaviour {
        TMP_Text text;

        private void Awake()
        {
            text = GetComponent<TMP_Text>();
        }

        IEnumerator Start()
        {
            yield return new WaitForSeconds(0.1f);

            Managers.Game.onActiveLevelChanged += OnLevelChanged;
            Managers.UI.popUps.onActivePopUpChange += OnPopUpChanged;
        }

        private void OnPopUpChanged(PopUpBase popUp)
        {
            if (popUp == null)
            {
                text.text = Managers.Game.ActiveLevel.name;
            }
            else
            {
                text.text = popUp.name;
            }
        }

        private void OnLevelChanged(Level newLevel)
        {
            text.text = newLevel.name;
        }
    }
}
