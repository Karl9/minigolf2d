﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Code {
    public class Fade : MonoBehaviour {

        Image image;

        private void Awake()
        {
            image = GetComponent<Image>();
            image.color = Color.black;
        }

        public IEnumerator FadeToColor(Color newColor, float time)
        {
            float oneTick = 0.05f;
            int ticks = (int)(time / oneTick);
            var currentColor = image.color;

            for (int i = 0; i < ticks; i++)
            {
                image.color = Color.Lerp(currentColor, newColor, i / (float)ticks);
                yield return new WaitForSeconds(oneTick);
            }

            image.color = newColor;
        }

    }
}
