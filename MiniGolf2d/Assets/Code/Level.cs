﻿using MiniGolf2d.Assets.Code.AllManagers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class Level : MonoBehaviour {
    Ball ball;
    Hole hole;
    List<Star> stars;

    [SerializeField] Transform cameraPos;
    
    int moves = 0;
    
	void Awake () {
        ball = GetComponentInChildren<Ball>();
        hole = GetComponentInChildren<Hole>();

        stars = GetComponentsInChildren<Star>().ToList();
    }

    void Update () {
		
	}

    [ContextMenu("Activate")]
    public void Activate() {
        gameObject.SetActive(true);
        Camera.main.transform.position = cameraPos.position;
        Restart();
    }

    public void Pause() {
        ball.Pause();
    }

    public void Resume() {
        ball.Resume();
    }

    public void Restart() {
        if (ball != null) {
            ball.transform.localPosition = Vector3.zero;
            var rigidbody = ball.GetComponent<Rigidbody2D>();
            if (rigidbody != null)
                rigidbody.velocity = Vector2.zero;
        }

        if (hole != null) {
            hole.Restart();
        }

        if (stars != null) {
            var starValues = GetStars();
            for (int i = 0; i < 3; i++) {
                if (i < stars.Count) {
                    var isStarCollected = starValues[i];
                    stars[i].gameObject.SetActive(!isStarCollected);
                    Managers.UI.inGameUI.SetStar(i, isStarCollected);
                }
            }
        }

        moves = 0;
        Managers.UI.inGameUI.SetMoves(moves);
    }

    internal void Deactivate() {
        gameObject.SetActive(false);
    }

    internal void AddStar(Star star) {
        int index = stars.IndexOf(star);
        Managers.UI.inGameUI.SetStar(index);
        Managers.Levels.SetStars(this, index);
    }

    public void AddMove() {
        moves++;
        Managers.UI.inGameUI.SetMoves(moves);
    }

    public int GetMoves() {
        return moves;
    }

    bool[] GetStars() {
        return Managers.Levels.GetStars(this);
    }

    public void LevelFinished() {
        Managers.Levels.SetMoves(this, moves);

        if (Managers.Levels.IsMenuLevel(this))
        {
            Restart();
        }
        else if (Managers.Levels.IsLastLevel(this))
        {
            Managers.UI.popUps.ActivateEndGamePopUp();
        }
        else
        {
            Managers.UI.popUps.ActivateLevelClearedPopUp();
        }
    }
}
