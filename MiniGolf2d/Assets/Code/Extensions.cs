using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Code {
    public static class Extensions {
        public static List<Transform> GetChildren(this Transform parent) {
            List<Transform> children = new List<Transform>();
            foreach (Transform child in parent) {
                children.Add(child);
            }
            return children;
        }

        public static void DestroyChildren(this Transform parent) {
            foreach (var t in parent.GetChildren()) {
                Component.Destroy(t.gameObject);
            }
        }

        public static bool IsNull(this System.Object systemObject) {
            return systemObject == null || systemObject.Equals(null);
        }

        public static bool IsNotNull(this System.Object systemObject) {
            return systemObject.IsNull() == false;
        }

        public static bool CanExecute<T>(this Action<T> action) {
            if (action.IsNull())
                return false;

            var methodReflectedType = action.Method.ReflectedType;
            var isMethodReflectedTypeStatic = methodReflectedType.IsStatic();
            if (action.Target == null && !isMethodReflectedTypeStatic)
                Debug.LogError("Null target and method is not static: " + methodReflectedType +
                               " - make that method static or think about not deleting object");
            return isMethodReflectedTypeStatic || action.Target.IsNotNull();
        }

        public static bool IsStatic(this Type method) {
            return method.GetProperties(BindingFlags.Static) != null;
        }
        
        public static T GetOrAddComponent<T>(this Component child) where T : Component {
            T result = child.GetComponent<T>();
            if (result == null) {
                result = child.gameObject.AddComponent<T>();
            }
            return result;
        }

        public static T GetOrAddComponent<T>(this GameObject child) where T : Component {
            T result = child.GetComponent<T>();
            if (result == null) {
                result = child.gameObject.AddComponent<T>();
            }
            return result;
        }
    }
}