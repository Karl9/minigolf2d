﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour {
    CircleCollider2D circleCollider2D;
    [SerializeField] float catchSpeed = 3;
    [SerializeField] float fallTime = 1.5f;
    [SerializeField] GameObject ballCage;

    [SerializeField] ParticleSystem particles;

    bool isBallInHole = false;

    float normalZPosition = 1f;
    float inHoleZPosition = -7f;

    Coroutine fallCoroutine;
    Coroutine particlesCoroutine;

    private void Start() {
        circleCollider2D = GetComponent<CircleCollider2D>();
    }

    void SetBallInHole() {
        isBallInHole = true;
        ballCage.SetActive(true);
        Managers.Audio.PlayBallInHole();
        fallCoroutine = StartCoroutine(Fall());
        particlesCoroutine = StartCoroutine(ParticlesCoroutine());
        Debug.Log("catch ball");
    }

    private IEnumerator ParticlesCoroutine() {
        if (particles != null) {
            particles.gameObject.SetActive(true);
            particles.Play();
            yield return new WaitForSeconds(1.5f);
            particles.gameObject.SetActive(false);
        } else {
            Debug.LogWarning("Particle system is null");
        }
    }

    private IEnumerator Fall() {
        const int ticks = 100;
        for (int i = 0; i < ticks; i++) {
            yield return new WaitForSeconds(fallTime / ticks);
            var newZ = Mathf.Lerp(0, inHoleZPosition, i / (float)ticks);
            ChangeZPosition(newZ);
        }

        ChangeZPosition(inHoleZPosition);
        Managers.Game.ActiveLevel.LevelFinished();
    }

    private void ChangeZPosition(float v) {
        var position = transform.position;
        position.z = v;
        transform.position = position;
    }

    public void Restart() {
        isBallInHole = false;

        if (fallCoroutine != null)
            StopCoroutine(fallCoroutine);

        ChangeZPosition(normalZPosition);
        ballCage.SetActive(false);
    }

    private void OnTriggerStay2D(Collider2D collision) {
        if (isBallInHole)
            return;

        var ball = collision.GetComponent<Ball>();
        if (ball != null && ball.Speed < catchSpeed) {
            var distance = Vector2.Distance(transform.position, ball.transform.position);
            //Debug.Log(string.Format("dist: {0} / {1}", distance + ball.ballSize/2, circleCollider2D.radius));
            if (distance + ball.ballSize/2 < circleCollider2D.radius) {
                SetBallInHole();
            }
        }
    }

}
