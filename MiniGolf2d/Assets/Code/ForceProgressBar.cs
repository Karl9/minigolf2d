﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceProgressBar : MonoBehaviour {
    [SerializeField] Ball ball;

    public Vector3 force;
    [ContextMenu("test")]
    void Test() {
        UpdateBar(force);
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void UpdateBar(Vector3 force) {
        transform.localPosition = force.normalized * ball.ballSize;

        transform.localScale = Vector3.one * force.magnitude;
        
        var angle = Vector3.SignedAngle(Vector3.up, force.normalized, Vector3.forward);
        var euler = new Vector3(0, 0, angle);

        transform.localRotation = Quaternion.Euler(euler);

    }
}
