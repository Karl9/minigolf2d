﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    [SerializeField] float hitForceBase = 10f;
    float maxDragLenght {
        get {
            return PlayerData.BallDragDistance;
        }
    }
    [SerializeField] ForceProgressBar forceProgressBar;

    public float ballSize = 0.5f;

    Vector3 mouseStart;
    Vector3 mouseEnd;

    Vector2 ballVelocityBackup;
    
    Rigidbody2D _rigidbody2D;
    public Rigidbody2D Rigidbody2D {
        get {
            if (_rigidbody2D == null)
                _rigidbody2D = GetComponent<Rigidbody2D>();
            return _rigidbody2D;
        }

        set {
            _rigidbody2D = value;
        }
    }

    public float Speed {
        get {
            var movement = GetComponentInChildren<MovementToRotation>();
            if (movement != null)
                return movement.speed;

            throw new MissingReferenceException("couldnt find movement");
        }
    }
    
    internal void Pause() {
        ballVelocityBackup = Rigidbody2D.velocity;
        Rigidbody2D.velocity = Vector2.zero;
        Rigidbody2D.isKinematic = true;
    }

    internal void Resume() {
        Rigidbody2D.velocity = ballVelocityBackup;
        Rigidbody2D.isKinematic = false;
    }

    void OnMouseDrag() {
        var force = GetForce(mouseStart, GetMousePositionWorld());
        forceProgressBar.UpdateBar(force);
    }

    void OnMouseDown() {
        mouseStart = GetMousePositionWorld();
        forceProgressBar.Show();
    }

    void OnMouseUp() {
        mouseEnd = GetMousePositionWorld();
        var force = GetForce(mouseStart, mouseEnd) * hitForceBase;
        HitBall(force);
    }

    private void HitBall(Vector3 force) {
        GetComponent<Rigidbody2D>().AddForce(force);
        Managers.Game.ActiveLevel.AddMove();
        Managers.Audio.PlayHitBall();
        forceProgressBar.Hide();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        Managers.Audio.PlayBallBounceOnce();
    }

    Vector3 GetForce(Vector3 start, Vector3 end) {
        var direction = end - start;
        direction *= -1;
        var force = Vector3.ClampMagnitude(direction, maxDragLenght) / maxDragLenght;
        return force;
    }

    static Vector3 GetMousePositionWorld() {
        var mousePositionWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePositionWorld.z = 0;
        return mousePositionWorld;
    }

    void OnDrawGizmos() {
        Vector3 end = mouseEnd;
        if (Input.GetMouseButton(0))
            end = GetMousePositionWorld();
        var direction = GetForce(mouseStart, end);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, transform.position + direction);
    }
}

