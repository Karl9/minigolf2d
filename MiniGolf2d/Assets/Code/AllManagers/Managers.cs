﻿using UnityEngine;
using System.Collections;

namespace MiniGolf2d.Assets.Code.AllManagers {
    [RequireComponent(typeof(GameManager))]
    [RequireComponent(typeof(UIManager))]
    [RequireComponent(typeof(LevelManager))]
    public class Managers : MonoBehaviour {
        private static GameManager _gameManager;
        public static GameManager Game {
            get { return _gameManager; }
        }

        private static UIManager _uiManager;
        public static UIManager UI {
            get { return _uiManager; }
        }

        private static LevelManager _levelManager;
        public static LevelManager Levels {
            get { return _levelManager; }
        }

        private static AudioManager _audioManager;
        public static AudioManager Audio {
            get { return _audioManager; }
        }

        void Awake() {
            _gameManager = GetComponent<GameManager>();
            _uiManager = GetComponent<UIManager>();
            _levelManager = GetComponent<LevelManager>();

            _audioManager = GetComponentInChildren<AudioManager>();

            DontDestroyOnLoad(gameObject);
        }
    }
}