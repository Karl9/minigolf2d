﻿using UnityEngine;
using System.Collections;
using System;

public class AudioManager : MonoBehaviour {
    public AudioSource musicSource;
	public AudioSource soundSource;
        
	public AudioClip gameMusic;
	public AudioClip uiClick;
	public AudioClip hitBall;

    public AudioClip ballInHole;
    public AudioClip ballBounce;
    public AudioClip collect;

    private void Start() {
        TurnSound(PlayerData.Sound);
        TurnMusic(PlayerData.Music);
    }

    #region Sound FX Methods
    public void PlayUIClick() {
        PlaySound(uiClick);
    }

    public void PlayHitBall() {
        PlaySound(hitBall);
    }

    public void PlayBallInHole() {
        if (soundSource.enabled)
            AudioSource.PlayClipAtPoint(ballInHole, Camera.main.transform.position);
        //PlaySound(ballBouncing);
    }

    public void PlayBallBounceOnce() {
        PlaySound(ballBounce);
    }

    public void PlayCollect() {
        PlaySound(collect);
    }

    private void PlaySound(AudioClip clip) {
        if (soundSource.enabled) {
            soundSource.clip = clip;
            soundSource.Play();
        }
    }

    internal void TurnSound(bool value) {
        soundSource.enabled = value;
    }
    #endregion

    #region Music Methods
    public void PlayGameMusic()
	{
		musicSource.clip = gameMusic;
		musicSource.Play ();
	}

	public void StopGameMusic()
	{
		musicSource.Stop ();
	}

    internal void TurnMusic(bool music) {
        if (music)
            PlayGameMusic();
        else
            StopGameMusic();
    }

    #endregion

}
