﻿using UnityEngine;
using System.Collections;
using MiniGolf2d.Assets.Code.UI;
using Code;

namespace MiniGolf2d.Assets.Code.AllManagers {
    public enum Menus {
        MAIN,
        INGAME
    }

    public class UIManager : MonoBehaviour {

        public MainMenu mainMenu;
        public InGameUI inGameUI;
        public PopUpsManager popUps;

        [SerializeField] Fade fade;
        [SerializeField] float fadeTime = 0.2f;
        
        private void Start() {
            mainMenu.gameObject.SetActive(true);
            inGameUI.gameObject.SetActive(false);
        }

        public void ActivateUI(Menus menutype) {
            popUps.HideActivePopup();

            if (menutype.Equals(Menus.MAIN)) {
                StartCoroutine(ActivateMainMenu());
            } else if (menutype.Equals(Menus.INGAME)) {
                StartCoroutine(ActivateInGameUI());
            }
        }

        IEnumerator ActivateMainMenu() {
            yield return fade.FadeToColor(Color.black, fadeTime);
            inGameUI.gameObject.SetActive(false);
            yield return null;
            mainMenu.gameObject.SetActive(true);
            yield return fade.FadeToColor(Color.clear, fadeTime);
        }

        IEnumerator ActivateInGameUI() {
            yield return fade.FadeToColor(Color.black, fadeTime);
            mainMenu.gameObject.SetActive(false);
            inGameUI.gameObject.SetActive(true);
            yield return fade.FadeToColor(Color.clear, fadeTime);
        }
    }
}
