﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.AllManagers {
    public class LevelManager : MonoBehaviour {
        List<Level> levels = new List<Level>();
        [SerializeField] Transform levelsParent;

        public List<Level> Levels {
            get { return levels; }
            set { levels = value; }
        }

        private void Start() {
            levels = levelsParent.GetComponentsInChildren<Level>().ToList();
            foreach (var level in levels) {
                level.gameObject.SetActive(false);
            }
            StartMenuLevel();
        }

        internal void StartMenuLevel() {
            DeactivatePrevLevel();
            Managers.Game.ActiveLevel = levels[0];
            Managers.UI.ActivateUI(Menus.MAIN);
            Managers.Game.ActiveLevel.Activate();
        }

        public void StartFirstLevel() {
            DeactivatePrevLevel();
            Managers.Game.ActiveLevel = levels[1];
            Managers.UI.ActivateUI(Menus.INGAME);
            Managers.Game.ActiveLevel.Activate();
        }

        internal void StartLevel(int index) {
            if (index >= levels.Count) {
                Debug.Log("no more levels");

            } else {
                DeactivatePrevLevel();
                Managers.Game.ActiveLevel = levels[index];
                Managers.UI.ActivateUI(Menus.INGAME);
                Managers.Game.ActiveLevel.Activate();
            }
        }

        private void DeactivatePrevLevel() {
            if (Managers.Game.ActiveLevel != null)
                Managers.Game.ActiveLevel.Deactivate();
        }

        internal void StartNextLevel() {
            int index = levels.IndexOf(Managers.Game.ActiveLevel);
            StartLevel(index + 1);
        }

        internal bool[] GetStars(Level level) {
            int index = levels.IndexOf(level);
            var stars = PlayerData.GetStars(index);
            return stars;
        }

        internal void SetStars(Level level, int starIndex, bool value = true) {
            int index = levels.IndexOf(level);
            PlayerData.SetStars(index, starIndex, value);
        }

        internal bool IsMenuLevel(Level level) {
            int index = levels.IndexOf(level);
            return index == 0;
        }

        internal bool IsLastLevel(Level level)
        {
            return levels.Last() == level;
        }

        internal int GetMinMoves(Level level) {
            int index = levels.IndexOf(level);
            var moves = PlayerData.GetMinMoves(index);
            return moves;
        }

        internal void SetMoves(Level level, int moves) {
            int index = levels.IndexOf(level);
            PlayerData.SetMoves(index, moves);
        }
    }
}
