﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private Level activeLevel;

    public Level ActiveLevel
    {
        get { return activeLevel; }
        set
        {
            activeLevel = value;
            onActiveLevelChanged(activeLevel);
        }
    }

    public Action<Level> onActiveLevelChanged = delegate { };

    private void Start() {
        Camera.main.orthographicSize = PlayerData.CameraOrtographicSize;
    }

}
