﻿[System.Serializable]
public class SettingsData {
    public static string key = "MiniGolf_Settings";

    public bool sound = true;
    public bool music = true;

    public float cameraOrtographicSize = 7f;
    public float ballDragDistance = 3f;
}