﻿using System;
using UnityEngine;

public class PlayerData {
    private static Data data;
    public static Data PData {
        get {
            if (data == null) {
                data = Load<Data>(Data.key);
                if (data == null) {
                    data = new Data();
                    SaveData();
                }
            }
            return data;
        }

        set {
            data = value;
            SaveData();
        }
    }
    
    private static SettingsData settings;
    public static SettingsData Settings {
        get {
            if (settings == null) {
                settings = Load<SettingsData>(SettingsData.key);
                if (settings == null) {
                    settings = new SettingsData();
                    SaveSettings();
                }
            }
            return settings;
        }

        set {
            settings = value;
            SaveSettings();
        }
    }

    public static bool Sound {
        get { return Settings.sound; }
        set {
            Settings.sound = value;
            SaveSettings();
        }
    }

    public static void ResetData() {
        PData = new Data();
    }

    internal static bool[] GetStars(int index) {
        return PData.GetStars(index);
    }

    internal static void SetStars(int levelIndex, int starIndex, bool value = true) {
        PData.SetStar(levelIndex, starIndex, value);
        SaveData();
    }

    internal static int GetMinMoves(int index) {
        return PData.GetMinMoves(index);
    }

    internal static void SetMoves(int index, int moves) {
        PData.SetMoves(index, moves);
        SaveData();
    }

    public static bool Music {
        get { return Settings.music; }
        set {
            Settings.music = value;
            SaveSettings();
        }
    }

    public static float CameraOrtographicSize {
        get { return Settings.cameraOrtographicSize; }
        set {
            Settings.cameraOrtographicSize = value;
            SaveSettings();
        }
    }

    public static float BallDragDistance {
        get { return Settings.ballDragDistance; }
        set {
            Settings.ballDragDistance = value;
            SaveSettings();
        }
    }
    
    public static void SaveSettings() {
        Save(settings, SettingsData.key);
    }

    public static void SaveData() {
        Save(data, Data.key);
    }


    private static void Save<T>(T data, string key) {
        string value = JsonUtility.ToJson(data);
        PlayerPrefs.SetString(key, value);
        Debug.Log("save data");
    }

    private static T Load<T>(string key) {
        string value = PlayerPrefs.GetString(key);
        Debug.Log("load data");
        return JsonUtility.FromJson<T>(value);
    }
}
