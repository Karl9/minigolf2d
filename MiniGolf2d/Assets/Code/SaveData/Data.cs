﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Data {
    public static string key = "MiniGolfData";

    public List<LevelData> levels = new List<LevelData>();

    public bool[] GetStars(int levelIndex) {
        AddMissingLevels(levelIndex);
        return levels[levelIndex].GetStars();
    }
    
    public void SetStar(int levelIndex, int starIndex, bool value = true) {
        AddMissingLevels(levelIndex);
        levels[levelIndex].SetStar(starIndex, value);
    }

    internal int GetMinMoves(int levelIndex) {
        AddMissingLevels(levelIndex);
        return levels[levelIndex].GetMinMoves();
    }

    internal void SetMoves(int levelIndex, int moves) {
        AddMissingLevels(levelIndex);
        levels[levelIndex].SetMoves(moves);
    }

    private void AddMissingLevels(int levelIndex) {
        if (levels == null)
            levels = new List<LevelData>();

        int levelsToAdd = levelIndex - (levels.Count - 1);
        if (levelsToAdd > 0) {
            AddLevels(levelsToAdd);
        }
    }
    
    private void AddLevels(int v) {
        for (int i = 0; i < v; i++) {
            levels.Add(new LevelData());
        }
    }
}

[System.Serializable]
public class LevelData {
    public bool star0;
    public bool star1;
    public bool star2;

    public int minMoves = int.MaxValue;

    public bool[] GetStars() {
        return new[] { star0, star1, star2 };
    }

    public void SetStar(int index, bool value = true) {
        switch (index) {
            case 0:
                star0 = value;
                break;
            case 1:
                star1 = value;
                break;
            case 2:
                star2 = value;
                break;
            default:
                Debug.LogError("Wrong index");
                break;
        }
    }

    internal void SetMoves(int moves) {
        if (moves < minMoves)
            minMoves = moves;
    }

    internal int GetMinMoves() {
        return minMoves;
    }
}