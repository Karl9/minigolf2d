﻿using MiniGolf2d.Assets.Code.AllManagers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Ball>()) {
            gameObject.SetActive(false);
            Managers.Game.ActiveLevel.AddStar(this);
            Managers.Audio.PlayCollect();
        }
    }
}
