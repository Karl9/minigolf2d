﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementToRotation : MonoBehaviour {
    [SerializeField] Ball ball;
    Vector3 prevPosition;
    float perimeter;

    public float speed;

    private void Start() {
        perimeter = 2 * Mathf.PI * ball.ballSize;
        prevPosition = transform.position;
    }

    private void Update() {
        var movement = transform.position - prevPosition;
        speed = movement.magnitude * Time.deltaTime * 10000f;

        prevPosition = transform.position;

        var movePercent = movement / perimeter;
        var moveAmount = movePercent * 360f;
        Vector3 euler = new Vector3(moveAmount.y, -moveAmount.x);
        transform.Rotate(euler, Space.World);
    }
}
