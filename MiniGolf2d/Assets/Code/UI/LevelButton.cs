﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGolf2d.Assets.Code.UI {
    public class LevelButton : MonoBehaviour {
        [SerializeField] TextMeshProUGUI text;

        LevelStarsUI starsUI;
        public LevelStarsUI StarsUI {
            get {
                if (starsUI == null)
                    starsUI = GetComponent<LevelStarsUI>();
                return starsUI;
            }

            set {
                starsUI = value;
            }
        }

        int index;

        UnityEngine.UI.Button button;
        public UnityEngine.UI.Button Button {
            get {
                if (button == null)
                    button = GetComponent<UnityEngine.UI.Button>();
                return button;
            }

            set {
                button = value;
            }
        }

        private void Start() {
            Button.onClick.AddListener(StartLevel);
        }

        private void StartLevel() {
            Debug.Log("start level " + index);
            Managers.Levels.StartLevel(index);
            Managers.Audio.PlayUIClick();
        }

        public void Load(int index) {
            this.index = index;
            text.text = index.ToString();
            var starsValues = PlayerData.GetStars(index);
            for (int i = 0; i < starsValues.Length; i++) {
                StarsUI.SetStar(i, starsValues[i]);
            }
        }
    }
}
