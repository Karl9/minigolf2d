﻿using MiniGolf2d.Assets.Code.AllManagers;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class PlayGameButton : ButtonScript {
        public override void A_Click() {
            Managers.Levels.StartFirstLevel();
            Managers.Audio.PlayUIClick();
        }
    }
}
