﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class LevelSelectPopupButton : ButtonScript {
        public override void A_Click() {
            Managers.UI.popUps.ActivateLevelSelectPopUp();
            Managers.Audio.PlayUIClick();
        }
    }
}
