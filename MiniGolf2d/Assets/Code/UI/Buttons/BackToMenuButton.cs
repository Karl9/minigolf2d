﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class BackToMenuButton : ButtonScript {
        public override void A_Click() {
            Managers.Levels.StartMenuLevel();
            Managers.Audio.PlayUIClick();
        }
    }
}
