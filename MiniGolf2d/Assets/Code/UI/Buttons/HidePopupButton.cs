﻿
using MiniGolf2d.Assets.Code.AllManagers;
using System;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class HidePopupButton : ButtonScript {
        public override void A_Click() {
            Managers.UI.popUps.HideActivePopup();
            Managers.Audio.PlayUIClick();
        }
    }
}
