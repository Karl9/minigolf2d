﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class OnOffButton : MonoBehaviour {
        bool isOn = false;
        public bool IsOn {
            get { return isOn; }
            set {
                isOn = value;
                offImage.SetActive(!isOn);
            }
        }

        Action buttonAction;
        Button button;
        [SerializeField] GameObject offImage;

        public void Init(Action buttonAction) {
            this.buttonAction = buttonAction;
            button = GetComponent<Button>();
            button.onClick.AddListener(TriggerAction);
        }

        private void TriggerAction() {
            IsOn = !IsOn;
            Managers.Audio.PlayUIClick();
            if (buttonAction != null)
                buttonAction();
        }
    }
}
