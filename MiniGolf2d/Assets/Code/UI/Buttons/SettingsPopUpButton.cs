﻿using MiniGolf2d.Assets.Code.AllManagers;
using System;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class SettingsPopUpButton : ButtonScript {
        public override void A_Click() {
            Managers.UI.popUps.ActivateSettingsPopUp();
            Managers.Audio.PlayUIClick();
        }
    }
}
