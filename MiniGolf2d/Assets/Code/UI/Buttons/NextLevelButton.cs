﻿using System;
using MiniGolf2d.Assets.Code.AllManagers;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class NextLevelButton : ButtonScript {
        public override void A_Click() {
            Managers.Levels.StartNextLevel();
            Managers.UI.popUps.HideActivePopup();
            Managers.Audio.PlayUIClick();
        }
    }
}