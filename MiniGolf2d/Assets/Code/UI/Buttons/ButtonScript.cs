﻿using UnityEngine;
using UnityEngine.UI;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public abstract class ButtonScript : MonoBehaviour {
        Button button;

        private void Awake() {
            button = GetComponent<Button>();
            button.onClick.AddListener(A_Click);
        }

        public abstract void A_Click();
    }
}