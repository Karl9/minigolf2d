﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code {
    public class UrlButton : MonoBehaviour {

        [SerializeField] string url;

        Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(ButtonClick);
        }

        private void ButtonClick()
        {
            Application.OpenURL(url);
        }
    }
}
