﻿
using System;
using MiniGolf2d.Assets.Code.AllManagers;

namespace MiniGolf2d.Assets.Code.UI.Buttons
{
    public class ResetDataButton : ButtonScript
    {
        public override void A_Click() {
            Managers.UI.popUps.ShowYesNoPopup(OnResetData, OnCancel, "You are going to delete all data.\n Are you sure?");
            Managers.Audio.PlayUIClick();
        }

        private void OnCancel() {
            Managers.UI.popUps.ActivateSettingsPopUp();
        }

        private void OnResetData() {
            PlayerData.ResetData();
            Managers.UI.popUps.ActivateSettingsPopUp();
        }
    }
}
