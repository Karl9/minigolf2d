﻿using System;
using MiniGolf2d.Assets.Code.AllManagers;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI.Buttons {
    public class RestartButton : ButtonScript {
        public override void A_Click() {
            Managers.Game.ActiveLevel.Restart();
            Managers.UI.popUps.HideActivePopup();
            Managers.Audio.PlayUIClick();
        }
    }
}
