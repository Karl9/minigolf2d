﻿using MiniGolf2d.Assets.Code.AllManagers;
using TMPro;
using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI {
    public class LevelClearedPopUp : PopUpBase {
        LevelStarsUI starsUI;
        public LevelStarsUI StarsUI {
            get {
                if (starsUI == null)
                    starsUI = GetComponentInChildren<LevelStarsUI>();
                return starsUI;
            }
            set { starsUI = value; }
        }

        [SerializeField] TextMeshProUGUI movesText;

        [SerializeField] TextMeshProUGUI minMovesText;


        private void OnEnable() {
            var activeLevelStars = Managers.Levels.GetStars(Managers.Game.ActiveLevel);
            StarsUI.SetStars(activeLevelStars);

            var moves = Managers.Game.ActiveLevel.GetMoves();
            movesText.text = string.Format("Your moves: {0}", moves);

            var minMoves = Managers.Levels.GetMinMoves(Managers.Game.ActiveLevel);
            minMovesText.text = string.Format("Best moves: {0}", minMoves);
        }
    }
}
