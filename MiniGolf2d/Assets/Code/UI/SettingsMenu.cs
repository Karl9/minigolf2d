﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MiniGolf2d.Assets.Code.UI.Buttons;
using System;
using MiniGolf2d.Assets.Code.AllManagers;

namespace MiniGolf2d.Assets.Code.UI {
    public class SettingsMenu : PopUpBase {
        [SerializeField] Slider cameraSizeSlider;
        [SerializeField] float minCameraSize = 10;
        [SerializeField] float maxCameraSize = 13;

        [SerializeField] Slider ballDragForceSlider;
        [SerializeField] float minDragLenght = 1;
        [SerializeField] float maxDragLenght = 5;

        [SerializeField] OnOffButton soundButton;
        [SerializeField] OnOffButton musicButton;

        private void Start() {
            soundButton.Init(TurnUpDownSound);
            soundButton.IsOn = PlayerData.Sound;

            musicButton.Init(TurnOnOffMusic);
            musicButton.IsOn = PlayerData.Music;

            cameraSizeSlider.onValueChanged.AddListener(CameraSliderDrag);
            cameraSizeSlider.value = Mathf.InverseLerp(minCameraSize, maxCameraSize, PlayerData.CameraOrtographicSize);

            ballDragForceSlider.onValueChanged.AddListener(BallDragForceSliderDrag);
            ballDragForceSlider.value = Mathf.InverseLerp(minDragLenght, maxDragLenght, PlayerData.BallDragDistance);
        }

        private void TurnOnOffMusic() {
            PlayerData.Music = !PlayerData.Music;
            Managers.Audio.TurnMusic(PlayerData.Music);
        }

        private void BallDragForceSliderDrag(float arg0) {
            PlayerData.BallDragDistance = Mathf.Lerp(minDragLenght, maxDragLenght, arg0);
        }

        private void CameraSliderDrag(float arg0) {
            PlayerData.CameraOrtographicSize = Mathf.Lerp(minCameraSize, maxCameraSize, arg0);
            Camera.main.orthographicSize = PlayerData.CameraOrtographicSize;
        }

        public void TurnUpDownSound() {
            PlayerData.Sound = !PlayerData.Sound;
            Managers.Audio.TurnSound(PlayerData.Sound);
        }


    }
}
