﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGolf2d.Assets.Code.UI {
    public class YesNoPopUp : PopUpBase {
        [SerializeField] Button yesButton;
        [SerializeField] Button noButton;

        [SerializeField] TextMeshProUGUI questionText;

        Action yesAction, noAction;

        private void Awake() {
            yesButton.onClick.AddListener(YesButtonClick);
            noButton.onClick.AddListener(NoButtonClick);
        }

        private void NoButtonClick() {
            if (noAction != null) {
                noAction();
            }
        }

        private void YesButtonClick() {
            if (yesAction != null) {
                yesAction();
            }
        }

        public void Show(Action yesAction = null, Action noAction = null, string text = "Are you Sure?") {
            questionText.text = text;
            this.yesAction = yesAction;
            this.noAction = noAction;
        }
    }
}
