﻿using UnityEngine;

namespace MiniGolf2d.Assets.Code.UI {
    public abstract class PopUpBase : MonoBehaviour {


        public void SetActive(bool active) {
            gameObject.SetActive(active);
        }
    }
}
