﻿using UnityEngine;
using System.Collections;
using MiniGolf2d.Assets.Code.AllManagers;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace MiniGolf2d.Assets.Code.UI {
    public class PopUpsManager : MonoBehaviour {
        public PopUpBase levelClearedPopUp;
        public PopUpBase settingsPopUp;
        public PopUpBase levelSelectPopUp;
        public YesNoPopUp yesNoPopUp;

        private PopUpBase activePopUp;

        public Button pupupPanel;

        public PopUpBase ActivePopUp
        {
            get { return activePopUp; }
            set
            {
                activePopUp = value;
                onActivePopUpChange(activePopUp);
            }
        }
        public Action<PopUpBase> onActivePopUpChange = delegate { };

        private void Awake() {
            levelClearedPopUp.SetActive(false);
            settingsPopUp.SetActive(false);
            levelSelectPopUp.SetActive(false);
            yesNoPopUp.SetActive(false);

            pupupPanel.gameObject.SetActive(false);
        }

        public void ActivateLevelClearedPopUp() {
            ShowPopup(levelClearedPopUp);
        }

        public void ActivateSettingsPopUp() {
            ShowPopup(settingsPopUp);
            Managers.Game.ActiveLevel.Pause();
        }

        public void ActivateLevelSelectPopUp() {
            ShowPopup(levelSelectPopUp);
            Managers.Game.ActiveLevel.Pause();
        }

        public void ShowYesNoPopup(System.Action yesAction = null, 
            System.Action noAction = null, string text = "Are you Sure?") {

            yesNoPopUp.Show(yesAction, noAction, text);
            ShowPopup(yesNoPopUp);
        }

        private void ShowPopup(PopUpBase popup) {
            HideActivePopup();
            pupupPanel.gameObject.SetActive(true);
            popup.SetActive(true);
            ActivePopUp = popup;
        }

        public void HideActivePopup() {
            if (ActivePopUp != null) {
                ActivePopUp.SetActive(false);
                pupupPanel.gameObject.SetActive(false);
                ActivePopUp = null;

                Managers.Game.ActiveLevel.Resume();
            }
        }

        internal void ActivateEndGamePopUp()
        {
            Debug.Log("Thanks for playing");
        }
    }
}
