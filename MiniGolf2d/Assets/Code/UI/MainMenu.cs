﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public GameObject menuButtons;

    void Awake() {
    }

    void OnEnable() {
        menuButtons.SetActive(true);
    }

    void OnDisable() {
        menuButtons.SetActive(false);
    }

    public void DisableMenuButtons() {
        menuButtons.SetActive(false);
    }
}

