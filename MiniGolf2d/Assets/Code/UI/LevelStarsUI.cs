﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGolf2d.Assets.Code.UI {
    public class LevelStarsUI : MonoBehaviour {
        public Color activeColor;
        public Color inactiveColor;

        public List<Image> stars;

        public void SetStar(int index, bool value = true) {
            stars[index].color = (value) ? activeColor : inactiveColor;
        }

        public void SetStars(bool[] starValues) {
            for (int i = 0; i < 3; i++) {
                if (i < stars.Count) {
                    var isStarCollected = starValues[i];
                    SetStar(i, isStarCollected);
                }
            }
        }
    }
}
