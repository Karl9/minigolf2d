﻿using UnityEngine;
using Code;

namespace MiniGolf2d.Assets.Code.UI {
    public class LevelSelectPopUp : PopUpBase {
        [SerializeField] LevelButton levelButtonPrefab;
        [SerializeField] Transform buttonsParent;

        private void OnEnable() {
            buttonsParent.transform.DestroyChildren();

            var levels = AllManagers.Managers.Levels.Levels;
            for (int i = 1; i < levels.Count; i++) {
                var levelButton = Instantiate(levelButtonPrefab);
                levelButton.transform.SetParent(buttonsParent, false);
                levelButton.Load(i);
            }
        }
    }
}