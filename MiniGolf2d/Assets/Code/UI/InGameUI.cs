﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MiniGolf2d.Assets.Code.UI;
using System;
using TMPro;

public class InGameUI : MonoBehaviour {
    LevelStarsUI starsUI;
    [SerializeField] TextMeshProUGUI movesText;

    private void Start() {
        starsUI = GetComponentInChildren<LevelStarsUI>();
    }

    public void SetStar(int index, bool value = true) {
        starsUI.SetStar(index, value);
    }

    internal void SetMoves(int moves) {
        movesText.text = moves.ToString();
    }
}
