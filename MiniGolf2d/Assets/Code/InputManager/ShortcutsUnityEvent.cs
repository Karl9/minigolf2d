﻿using UnityEngine;
using UnityEngine.Events;

namespace Code.InputManager {
    public class ShortcutsUnityEvent : MonoBehaviour {
        public InputAction shortcut = new InputAction();

        public UnityEvent buttonDownEvent;
        public UnityEvent buttonUpEvent;
        public UnityEvent buttonPressedEvent;

        void Update() {
            if (shortcut.IsNotNull()) {
                if (shortcut.ActionDown() && buttonDownEvent.IsNotNull())
                    buttonDownEvent.Invoke();

                if (shortcut.ActionUp() && buttonUpEvent.IsNotNull())
                    buttonUpEvent.Invoke();

                if (shortcut.ActionPressed() && buttonPressedEvent.IsNotNull())
                    buttonPressedEvent.Invoke();
            }
        }
        
        public void ChangeActive(GameObject gameObject) {
            gameObject.SetActive(!gameObject.activeInHierarchy);
        }

        public override string ToString() {
            return gameObject.name + " - " + shortcut;
        }
    }
}