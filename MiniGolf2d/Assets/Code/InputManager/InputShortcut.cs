using UnityEngine;

namespace Code.InputManager {
    [System.Serializable]
    public class InputShortcut {
        public KeyCode key;
        public bool shift;
        public bool ctrl;
        public bool alt;

        bool SupportKeysPressed() {
            return Implication(shift, ShiftPressed()) && 
                   Implication(ctrl, CtrlPressed()) &&
                   Implication(alt, AltPressed());
        }

        bool ShiftPressed() {
            return Input.GetKey(KeyCode.LeftShift) | Input.GetKey(KeyCode.RightShift);
        }

        bool AltPressed() {
            return Input.GetKey(KeyCode.LeftAlt) | Input.GetKey(KeyCode.RightAlt);
        }

        bool CtrlPressed() {
            return Input.GetKey(KeyCode.LeftControl) | Input.GetKey(KeyCode.RightControl);
        }

        bool Implication(bool p, bool q) {
            return !p | q;
        }

        public bool ShortcutUp() {
            return SupportKeysPressed() && Input.GetKeyUp(key);
        }

        public bool ShortcutDown() {
            return SupportKeysPressed() && Input.GetKeyDown(key);
        }

        public bool ShortcutPressed() {
            return SupportKeysPressed() && Input.GetKey(key);
        }

        public void TryCatchKeysCombination(ref bool catchKey, bool editCombination = true) {
            Event e = Event.current;
            if (e != null) {
                if (e.type == EventType.KeyDown && e.keyCode != KeyCode.None) {
                    e.Use();
                }

                if (e.type == EventType.KeyUp && e.keyCode != KeyCode.None) { // doit add shift shortcuts 
                    // doit add mouse keys
                    // doit add hardcoded shortcuts ctrl+z, ctrl+b
                    Debug.Log(string.Format("event : {0}, {1}, {2}, {3}, {4}", e, e.alt, e.control, e.shift,
                        e.keyCode));

                    if (editCombination) {
                        shift = e.shift;
                        ctrl = e.control;
                        alt = e.alt;
                    }
                    key = e.keyCode;
                    e.Use();
                    catchKey = false;
                }
            }
        }

        public override string ToString() {
            var shiftText = shift ? "Shift+" : "";
            var ctrlText = ctrl ? "Ctrl+" : "";
            var altText = alt ? "Alt+" : "";
            return shiftText + ctrlText + altText + key;
        }
    }
}