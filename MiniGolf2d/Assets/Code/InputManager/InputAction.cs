﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.InputManager {
    [System.Serializable]
    public class InputAction {
        [HideInInspector]
        public List<InputShortcut> keys;

        public bool ActionPressed() {
            return keys.Any(x => x.ShortcutPressed());
        }

        public bool ActionUp() {
            return keys.Any(x => x.ShortcutUp());
        }

        public bool ActionDown() {
            return keys.Any(x => x.ShortcutDown());
        }

        public override string ToString() {
            return string.Join(", ", keys.Select(x => x.ToString()).ToArray());
        }
    }
}