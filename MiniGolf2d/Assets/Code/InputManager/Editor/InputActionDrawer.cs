﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Code.InputManager.Editor {
    [CustomPropertyDrawer(typeof(InputAction))]
    public class InputActionDrawer : PropertyDrawer {
        bool showList = true;

        bool initialized = false;
        bool catchKey;
        bool editCombination;

        int selectedIndex = 0;

        ReorderableList reorderableList;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (!initialized || reorderableList == null)
                Initialize(property);

            showList = EditorGUI.Foldout(new Rect(position.x, position.y, 30, 15), showList, showList ? string.Empty : property.name);
            if (showList) {
                if (reorderableList != null) {
                    property.serializedObject.Update();
                    reorderableList.DoList(position);
                    property.serializedObject.ApplyModifiedProperties();
                }
            
                if (catchKey) {
                    GetCombination(property);
                }
            }
        }

        void GetCombination(SerializedProperty property) {
            var targetField = InputShortcutDrawer.GetFieldInfo(property);
            var inputAction = (InputAction)targetField.GetValue(property.serializedObject.targetObject);

            if (inputAction.keys.Count > selectedIndex)
                inputAction.keys[selectedIndex].TryCatchKeysCombination(ref catchKey, editCombination);
            else {
                catchKey = false;
                selectedIndex = 0;
            }
        }

        void Initialize(SerializedProperty property) {
            initialized = true;
            //set up lists and cache data and whatnot
            var list = property.FindPropertyRelative("keys");
            reorderableList =
                new ReorderableList(property.serializedObject, list, true, true, true, true) {
                    drawElementCallback = DrawElementCallback,
                    drawHeaderCallback = (Rect rect) => {
                        EditorGUI.LabelField(rect, property.name);
                    },
                    onSelectCallback = SelectCallback
                };
        }

        void SelectCallback(ReorderableList list) {
            selectedIndex = list.index;
        }

        void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused) {
            if (catchKey && selectedIndex == index) {
                EditorGUI.LabelField(MakeRect(ref rect, 300), "waiting for key/keys combination");
            } else {
                
                var rectCopy = rect;
                var element = reorderableList.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
                EditorGUI.LabelField(MakeRect(ref rectCopy, 30), index + " -");

                PropertyWithLabel(ref rectCopy, element, "shift");
                PropertyWithLabel(ref rectCopy, element, "ctrl");
                PropertyWithLabel(ref rectCopy, element, "alt");
                EditorGUI.PropertyField(MakeRect(ref rectCopy, 100), element.FindPropertyRelative("key"), GUIContent.none);


                if (GUI.Button(MakeRect(ref rectCopy, 120), "edit combination")) {
                    selectedIndex = index;
                    catchKey = true;
                    editCombination = true;
                }

                if (GUI.Button(MakeRect(ref rectCopy, 100), "edit key")) {
                    selectedIndex = index;
                    catchKey = true;
                    editCombination = false;
                }
            }
        }

        void PropertyWithLabel(ref Rect rectCopy, SerializedProperty element, string name) {
            EditorGUI.LabelField(MakeRect(ref rectCopy, 30), name);
            EditorGUI.PropertyField(MakeRect(ref rectCopy, 30), element.FindPropertyRelative(name), GUIContent.none);
        }

        static Rect MakeRect(ref Rect rect, int width) {
            var newRect = new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight);
            rect.x += width;
            return newRect;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float reorderableHeight = 0f;
            if (reorderableList != null && showList) {
                reorderableHeight = 55 + Mathf.Max(reorderableList.count - 1, 0) * 21;
            }

            return base.GetPropertyHeight(property, label) + reorderableHeight;
        }
    }
}