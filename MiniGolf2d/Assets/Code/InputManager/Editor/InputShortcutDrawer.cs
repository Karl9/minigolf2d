﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Code.InputManager.Editor {
    [CustomPropertyDrawer(typeof(InputShortcut))]
    public class InputShortcutDrawer : PropertyDrawer {
        bool catchKey = false;
        bool loaded = false;

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var rectCopy = position;
            if (Event.current.type == EventType.Layout || Event.current.type == EventType.Repaint) {
                //Done...
                loaded = true;
            }

            if (loaded) {
                // Using BeginProperty / EndProperty on the parent property means that
                // prefab override logic works on the entire property.
                EditorGUI.BeginProperty(position, label, property);
                // Draw label
                EditorGUI.PrefixLabel(MakeRect(ref rectCopy, 90), label);
                // Don't make child fields be indented
                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;

                // Draw fields - passs GUIContent.none to each so they are drawn without labels
                if (GetFieldValue(property).IsNull()) {
                    EditorGUI.LabelField(MakeRect(ref rectCopy, 50), ": 'null'");
                    if (GUI.Button(MakeRect(ref rectCopy, 100), "new")) {
                        var fieldInfo = GetFieldInfo(property);
                        fieldInfo.SetValue(property.serializedObject, new InputShortcut());
                    }
                }
                else if (catchKey) {
                    EditorGUI.LabelField(MakeRect(ref rectCopy, 300), "input key combination");
                } else {

                    EditorGUIUtility.labelWidth = 30.0f;
                    PropertyWithLabel(ref rectCopy, property, "shift");
                    PropertyWithLabel(ref rectCopy, property, "ctrl");
                    PropertyWithLabel(ref rectCopy, property, "alt");
                    PropertyWithLabel(ref rectCopy, property, "key");


                    if (GUI.Button(MakeRect(ref rectCopy, 100), "edit")) {
                        catchKey = true;
                    }
                }
                
                // Set indent back to what it was
                EditorGUI.indentLevel = indent;

                EditorGUI.EndProperty();

            }
            if (catchKey && GetFieldValue(property).IsNotNull())
                GetCombination(property);
        }

        void PropertyWithLabel(ref Rect rectCopy, SerializedProperty property, string name) {
            EditorGUI.LabelField(MakeRect(ref rectCopy, 30), name);
            EditorGUI.PropertyField(MakeRect(ref rectCopy, 30), property.FindPropertyRelative(name), GUIContent.none);
        }

        static Rect MakeRect(ref Rect rect, int width) {
            var newRect = new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight);
            rect.x += width;
            return newRect;
        }

        void GetCombination(SerializedProperty property) {
            FieldInfo targetField = GetFieldInfo(property);
            var inputShortcut = (InputShortcut)targetField.GetValue(property.serializedObject.targetObject);
            inputShortcut.TryCatchKeysCombination(ref catchKey);
        }

        public static FieldInfo GetFieldInfo(SerializedProperty property) {
            var targetObject = property.serializedObject.targetObject;
            var type = targetObject.GetType();
            var targetField = type.GetField(property.name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            return targetField;
        }

        public static InputShortcut GetFieldValue(SerializedProperty property) {
            FieldInfo targetField = GetFieldInfo(property);
            var inputShortcut = (InputShortcut)targetField.GetValue(property.serializedObject.targetObject);
            return inputShortcut;
        }
    }
}