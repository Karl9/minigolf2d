﻿using UnityEngine;

namespace Code {
    public class PauseScreen : MonoBehaviour {

        private void OnApplicationFocus(bool focus)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(!focus);
            }
        }
    }
}
