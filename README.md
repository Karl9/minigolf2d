## About

MiniGolf is 2d golf like game where player task is to knock the ball into the hole.

![GIF](https://i.imgur.com/6l4f4JU.gif)

## Running

Required Unity 2017.2

Unity 2017.1 or previous are not recomended. 

To play in editor just open scene 
```sh
Assets/Scenes/game.unity 
```
and hit play.

## Builds

Android:
https://drive.google.com/file/d/1ErNjcKTwRl-wcD07hmRtHLbXI3bP6slb/view?usp=sharing

Windows:
https://drive.google.com/file/d/1a-3FY2E2Zka5p0ZPuKBfkrlTAI4vVDJg/view?usp=sharing

## Screenshots

![screen](https://i.imgur.com/AvilvrN.png)
![screen](https://i.imgur.com/v24h34Z.png)
![screen](https://i.imgur.com/dcIJkxo.png)
![screen](https://i.imgur.com/LWrGhwx.png)
![screen](https://i.imgur.com/saVKc09.png)